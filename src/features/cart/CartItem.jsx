import { useDispatch } from "react-redux";
import Button from "../../ui/Button";
import { formatCurrency } from "../../utils/helpers";
import { deleteItem } from "./cartSlice";
import UpdateCartQuantity from "./UpdateCartQuantity";

const CartItem = ({ item, id }) => {
  const { pizzaId, name, quantity, totalPrice } = item;
  const dispatch = useDispatch();
  return (
    <li className="flex justify-between items-center py-4">
      <p className="mb-1">
        {quantity}&times; {name}
      </p>
      <div className="flex items-center space-x-4 font-bold">
        <p>{formatCurrency(totalPrice)}</p>
        <UpdateCartQuantity id={pizzaId}/>
        <Button type={"small"} onClick={()=>dispatch(deleteItem(pizzaId))}>
          Delete
        </Button>
      </div>
    </li>
  );
}

export default CartItem;
