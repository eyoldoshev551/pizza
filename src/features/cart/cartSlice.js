import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  cart: [
    // {
    //   pizzaId: 1234,
    //   name: "Margaritha",
    //   unitPrice: 20,
    //   quantity: 2,
    //   totalPrice: 40,
    // },
  ],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    deleteItem: (state, action) => {
      state.cart = state.cart.filter((pizza) =>  pizza.pizzaId != action.payload);
    },
    addItem: (state, action) => {
      state.cart = [...state.cart, action.payload];

    },
    clearItem: (state, action) => {
      state.cart = [];
    },
    increeseCartQuantity: (state, action) => {
      const item = state.cart.find((item) => {
        return item.pizzaId == action.payload
      });
      item.quantity++;
      item.totalPrice = item.quantity * item.unitPrice
    },
    decreeseCartQuantity: (state, action) => {
      const item = state.cart.find((item) => {
        return item.pizzaId == action.payload
      });
      item.quantity--;
      item.totalPrice = item.quantity * item.unitPrice;
      if (item.quantity < 1) {
        cartSlice.caseReducers.deleteItem(state, action);

      }
    },
  },
});

export const {
  addItem,
  deleteItem,
  clearItem,
  decreeseCartQuantity,
  increeseCartQuantity,
} = cartSlice.actions;

export default cartSlice.reducer;

export function getTotalCartPQty(state) {
  return state.cart.cart.reduce(
    (prev, current) => (prev += current.quantity),
    0
  );
}
export function getTotalCartPrice(state) {
  return state.cart.cart.reduce(
    (prev, current) => (prev += current.totalPrice),
    0
  );
}

export const getCurrentItemQuantity = (id) => (state) => {
  return state.cart.cart.find((item) => item.pizzaId == id)?.quantity || 0;
}