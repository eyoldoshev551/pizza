import React from "react";
import CartOverview from "../features/cart/CartOverview";
import { Link, Outlet, useNavigation } from "react-router-dom";
import Spinner from "../ui/Spinner";
import Header from "./Header";
const AppLayout = () => {
  const navigation = useNavigation();

  return (
    <>
      {navigation.state == "loading" && <Spinner />}
      <div className="grid grid-rows-[auto_1fr_auto] h-screen font-pizza">
        <Header />
        {/* <Link to={"/menu"}>Go to menu</Link> */}
        <div className="overflow-y-auto">
          <main className="max-w-3xl mx-auto">
            <Outlet />
          </main>
        </div>
        <CartOverview />
      </div>
    </>
  );
};

export default AppLayout;
