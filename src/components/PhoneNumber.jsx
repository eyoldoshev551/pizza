import React, { useState } from 'react';

const PhoneNumber = () => {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [valid, setValid] = useState(true);

  const handleChange = (event) => {
    const input = event.target.value;
    setPhoneNumber(input);
    setValid(validatePhoneNumber(input));
  };

  const validatePhoneNumber = (phoneNumber) => {
    const phoneNumberPattern = /^\d{10}$/; // Validates a 10-digit phone number

    return phoneNumberPattern.test(phoneNumber);
  };

  return (
    <div>
      <label>
        Phone Number:
        <input
          type="text"
          value={phoneNumber}
          onChange={handleChange}
        />
      </label>
      {!valid && <p>Please enter a valid 10-digit phone number.</p>}
    </div>
  );
};

export default PhoneNumber;